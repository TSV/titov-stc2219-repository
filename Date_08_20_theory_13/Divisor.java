package Date_08_20_theory_13;
/* непонятно, что на что делить, поэтому делить будем всё на всё */
public class Divisor {
    /*
      вот тут  вообще хз что на входе, что на выходе.
      В условии не сказано, СКОЛЬКО чисел давать методу.
      Значит, будет отчёт в текстовом виде
    */
    public static String divisor(int ... data){
        System.out.println("\n"+"Товарищ Сталин, что Вы курите? Пункт 4 задания предельно понятен :)"+"\n"
                +"https://www.youtube.com/watch?v=M4uTC_5r6eM"+"\n"+
                "нужно много котов и оконные рамы"+"\n"
                +"\n"+"Будет создан  отчёт в текстовом виде"+"\n");
        String res = "";
        if (data.length == 1) return "1";
        for (int i = 0; i < data.length-1; i++) {
            for (int j = i+1; j < data.length ; j++) {
                res+= "Делим "+data[i]+" и "+data[j] + " в разных позах из условия задачи. ";
                if (!((data[i]>data[j]) || (data[j]>0)) ){
                    res += "Сначала получилось "+sokraschator(data[i],data[j])+".";}
                    else{ res += "Сначала не получилось.";}
                if (!((data[j]>data[i]) || (data[i]>0)) ){
                    res += " А потом получилось "+sokraschator(data[j],data[i])+".";}
                 else{ res += " А потом не получилось.";}

                res += "\n";
            }
        }
        return res;
    }
    public static String sokraschator(int a,int b){
        if (b == 0) return "BLACK INFINITY";
        if (a == 0) return " 0 ";
        if (a == b) return " 1 ";
        int minus = 1;
        if (a*b<0) minus = -1;
        a = Math.abs(a); b=Math.abs(b);
        if (b%a == 0) return " "+minus+"/"+b/a+" ";
        if (a%b ==0 )return " "+(minus*a)/b+" ";

        int del = (int) (Math.max(a,b))/2;

        while (del >= 1 ){
            if (a%del == 0 && b%del == 0){a/=del;b/=del;}
            del-=1;}

        return " "+minus*a+"/"+b+" ";
    }

//    public static void main(String[] args) {
//        System.out.println(sokraschator(15,45));
//    }
}
