package Date_08_20_theory_13;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SubtractorTest {
    @Test
    void sub() {
        int test = Subtractor.substance(1, 2, 3, 4, 5, 76, 7, 8);
        assert test == 46;
    }

    @Test
    void subs() {
        int test = Subtractor.substance(-1, -2, -3, -4, -5, 0, -7, -8);
        assert test == 30;
    }
    @Test
    void subst() {
        int test = Subtractor.substance(1, 2, 3, 4, 5, -76, 7, 8);
        assert test == 62;
    }
}
