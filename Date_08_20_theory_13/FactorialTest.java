package Date_08_20_theory_13;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FactorialTest {

    @Test
    void fctr() {
    assert Factorial.fctr(5)==120;
    }
    @Test
    void fctr1() {
    assert Factorial.fctr(-2)==0;
    }
    @Test
    void fctr2() {
    assert Factorial.fctr(1)==1;
    }
    @Test
    void fctr3() {
    assert Factorial.fctr(7)==5040;
    }
}