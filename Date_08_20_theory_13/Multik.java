package Date_08_20_theory_13;

public class Multik {
    public static long multik(int ... data){
        long res = 1;
        for (int i = 0; i < data.length; i++) {
            res *= data[i];
        }
        return res;
    }
}
