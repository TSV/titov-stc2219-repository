package Date_08_20_theory_13;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DivisorTest {
    @Test
    void reduce1 (){
        String test = Divisor.sokraschator(7,8);
        assert test.equals(" 7/8 ");
    }
    @Test
    void reduce2 (){
        String test = Divisor.sokraschator(7,0);
        assert test.equals("BLACK INFINITY");
    }
    @Test
    void reduce3 (){
        String test = Divisor.sokraschator(0,7);
        assert test.equals(" 0 ");
    }
    @Test
    void reduce4 (){
        String test = Divisor.sokraschator(15,15);
        assert test.equals(" 1 ");
    }
    @Test
    void reduce5 (){
        String test = Divisor.sokraschator(15,45);
        assert test.equals(" 1/3 ");
    }
    @Test
    void reduce6 (){
        String test = Divisor.sokraschator(15,5);
        assert test.equals(" 3 ");
    }
    @Test
    void reduce7 (){
        String test = Divisor.sokraschator(57,-38);
        assert test.equals(" -3/2 ");
    }
    @Test
    void reduce8 (){
        String test = Divisor.sokraschator(-57,-38);
        assert test.equals(" 3/2 ");
    }
    @Test
    void reduce9 (){
        String test = Divisor.sokraschator(-57,38);
        assert test.equals(" -3/2 ");
    }


}