package Date_08_20_theory_13;

public class Factorial {
    public static int fctr(int a){
        if (a<1) return 0;
        if (a == 1) return 1; else return a*fctr(a-1);
    }
}
