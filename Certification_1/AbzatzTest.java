package Certification_1;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;


class AbzatzTest {
private static final String testline ="Во поле берёза стояла. Во поле кудрявая стояла.";
    @Test
    void words() {
        ArrayList<Object> t = Abzatz.words(testline);
        assert ((ArrayList<String>) t.get(0)).size() == 5;
    }

    @Test
    void symbols() {
        ArrayList<Object> t = Abzatz.symbols(testline);
        assert ((ArrayList<String>) t.get(0)).size() == 17;
    }
}