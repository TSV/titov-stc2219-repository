package Certification_1;

public class Main {
    public static void main(String[] args) {
        Abzatz.initData();
        System.out.println(Messages.hello);
        String source =Abzatz.selectSource();
        System.out.println("Про слова: ");
        Abzatz.report(Abzatz.words(source),Abzatz.wordsReport);
        System.out.println("\n"+"-----------------------------------------------------------------"+"\n");
        System.out.println("Про буквы: ");
        Abzatz.report(Abzatz.symbols(source),Abzatz.symbolReport);
    }
}
