package Certification_1;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Abzatz {
    private static final String dirPath = "Certification_1\\files";
    private static final String demoFileName = "demo.txt";
    private static final String reportWordsFileName = "report_words.txt";
    private static final String reportSymbFileName = "report_symb.txt";

    private static final File dir = new File(dirPath);
    private static final File demo = new File(dirPath + "\\" + demoFileName);
    public static final File wordsReport = new File(dirPath + "\\" + reportWordsFileName);
    public static final File symbolReport = new File(dirPath + "\\" + reportSymbFileName);

    private static final Set<String> userSayDemo = new HashSet<>(Arrays.asList("D", "d", "Д", "д", "В", "в"));

    private static final Set<String> userSayFile = new HashSet<>(Arrays.asList("F", "f", "Ф", "ф", "А", "а"));

    private static boolean isDirExist(File dir) {
        return dir.exists() && dir.isDirectory();
    }

    private static boolean isFileExist(File data) {
        return data.exists() && data.isFile();
    }

    public static void initData() {
        if (!isDirExist(dir)) {
            new File(dirPath).mkdir();
            try {
                if (demo.createNewFile()) {
                    System.out.println(Messages.fileCreated);
                }
            } catch (IOException e) {
                throw new RuntimeException();
            }
        } else if (!isFileExist(demo)) {
            try {
                if (demo.createNewFile()) {
                    System.out.println(Messages.fileCreated);
                }
            } catch (IOException e) {
                throw new RuntimeException();
            }
        }
        if (!isFileExist(wordsReport)) {
            try {
                if (wordsReport.createNewFile()) {
                    System.out.println(Messages.fileCreated);
                }
            } catch (IOException e) {
                throw new RuntimeException();
            }
        }
        if (!isFileExist(symbolReport)) {
            try {
                if (symbolReport.createNewFile()) {
                    System.out.println(Messages.fileCreated);
                }
            } catch (IOException e) {
                throw new RuntimeException();
            }
        }

        try (Writer writer = new FileWriter(demo, false)) {
            writer.write(Messages.demo);
        } catch (IOException e) {
            throw new RuntimeException();
        }

    }

    private static String openUserFile(String userFileName) {
        String text = null;
        try {
            text = new String(Files.readAllBytes(Paths.get(userFileName)));
        } catch (java.nio.file.InvalidPathException e) {
            System.out.println(Messages.fileNameError);
        } catch (IOException e) {
            System.out.println(Messages.fileNotFound);
        }
        return text;
    }

    private static String openDefaultFile() {
        String text = null;
        try {
            text = new String(Files.readAllBytes(Paths.get(demo.toURI())));
        } catch (IOException e) {
            System.out.println("Файл не найден");
        }
        return text;
    }

    private static String stringAdapt(String s) {
        s = s.replace("\r\n", " ");
        s = s.toLowerCase();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (Character.isLetter(s.charAt(i)) || Character.isSpaceChar(s.charAt(i))) {
                result.append(s.charAt(i));
            }
        }
        return result.toString();
    }

    private static void cleanArray(List<String> list) {
        int garbageCount = Collections.frequency(list, "");
        for (int i = 0; i < garbageCount; i++) {
            list.remove("");
        }
    }

    public static ArrayList<Object> words(String string) {
        ArrayList<Object> result = new ArrayList<>();
        if (string == null) {
            System.out.println("Была введена пустая строка.");
        } else {
            List<String> wrds = new ArrayList<>(Arrays.asList(string.split(" ")));
            cleanArray(wrds);
            Collections.sort(wrds);

            HashMap<String, Integer> wrdsMap = new HashMap<>();
            for (String wrd : wrds) {
                wrdsMap.put(wrd, 0);
            }
            Set<String> wrdsSet = wrdsMap.keySet();

            ArrayList<String> wrdsList = new ArrayList<>(wrdsSet);

            Collections.sort(wrdsList);
            for (String w : wrdsSet) {
                wrdsMap.put(w, Collections.frequency(wrds, w));
            }
            result.add(wrdsList);
            result.add(wrdsMap);
//            report(wrdsList, wrdsMap,wordsReport);
        }
        return result;
    }

    public static ArrayList<Object> symbols(String string) {
        ArrayList<Object> result = new ArrayList<>();
        if (string == null) {
            System.out.println("Была введена пустая строка.");
        } else {
            ArrayList<String> list = new ArrayList<>();
            for (int i = 0; i < string.length(); i++) {
                if (Character.isLetter(string.charAt(i))) {
                    list.add(String.valueOf(string.charAt(i)));
                }
            }
            Set<String> symbolSet = new HashSet<>(list);
            ArrayList<String> symbolList = new ArrayList<>(symbolSet);
            Collections.sort(symbolList);
            HashMap<String, Integer> symbolMap = new HashMap<>();
            for (String s : symbolList) {
                symbolMap.put(s, Collections.frequency(list, s));
            }
            result.add(symbolList);
            result.add(symbolMap);
            //report(symbolList,symbolMap,symbolReport);
        }
        return result;
    }

    public static void report(ArrayList<Object> list, File file) {
        ArrayList<String> keys = (ArrayList<String>) list.get(0);
        HashMap<String, Integer> map = (HashMap<String, Integer>) list.get(1);

        try (Writer writer = new FileWriter(file, false)) {
            for (String w : keys) {
                String string = w + " --> " + map.get(w);
                System.out.println(string);
                string += "\n";
                writer.write(string);
            }
        } catch (IOException e) {
            throw new RuntimeException();
        }

    }

    private static final Scanner console = new Scanner(System.in);

    public static String selectSource() {
        System.out.println(Messages.select_src);
        String text = console.nextLine();
        if (userSayDemo.contains(text)) {
            text = openDefaultFile();
        } else if (userSayFile.contains(text)) {
            System.out.println(Messages.inputFileName);
            String userFileName = console.nextLine();
            text = openUserFile(userFileName);
        }
        if (text != null) {
            text = stringAdapt(text);
        }
        return text;
    }
}
