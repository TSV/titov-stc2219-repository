package Date_08_15_theory_04;

public class Square extends Rectangle implements Movable{
//поля
/*     суперполя    */
//конструкторы
    Square(double x,double y,double width){
        super(x,y,width);
        this.height= width; // проверил без этой строки, площадь 0
    }

//методы

    @Override
    double getPerimetr() {
        return 4*getWidth();
    }
    @Override
    public void move(double x, double y){
        System.out.println("Old coordinates: X = "+getX()+" Y = "+getY());
        setX(x); setY(y);
        System.out.println("New coordinates: X = "+getX()+" Y = "+getY());
    }
}
