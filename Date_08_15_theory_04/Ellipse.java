package Date_08_15_theory_04;

public class Ellipse extends Figure {
//поля
    private double width; double height;

//конструкторы
//  на 4 параметра
    Ellipse(double x, double y, double width, double height){
        super(x,y);
        this.width = width;
        this.height = height;
        assert width > 0;
        assert height >0;
        /*  есть мысль сюда всесто assert вставить if с проверкой,
            что число положительное, иначе ширина и высота в ноль,
            и сообщение типа создана фигура величины 0".
            Не будет ли такой подход маскировкой ошибки?
        */
    }
// на 3 параметра

    public Ellipse(double x, double y, double width) {
        super(x, y);
        this.width = width;
        assert width > 0;
    }


//методы

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
// pi не стал делать наследуемым полем, а вдруг будет не нужно
    @Override
    double getPerimetr(){
        final double pi = Math.PI;
        return 4*(width*width+height*height+width*height*(pi-2))/(width+height);
    }
}
