package Date_08_15_theory_04;

public class Circle extends Ellipse implements Movable{
//поля
    //на 3 параметра исправил
    public Circle(double x, double y, double width) {

        super(x,y,width);this.height = width;
    }
//методы

    @Override
    double getPerimetr() {
    final double pi = Math.PI;
    return pi*getWidth()*getWidth();
    }
    @Override
    public void move(double x, double y){
        System.out.println("Old coordinates: X = "+getX()+" Y = "+getY());
        setX(x); setY(y);
        System.out.println("New coordinates: X = "+getX()+" Y = "+getY());
    }
}
