package Date_08_15_theory_04;


public class Rectangle extends Figure{
//поля
    private  double width; double height;
//конструкторы
//  на 4 параметра
    Rectangle (double x,double y,double width,double height){
        super( x, y);
        this.width = width;
        this.height = height;
        assert width > 0;
        assert height > 0;
        /*  есть мысль сюда всесто assert вставить if с проверкой,
          что число положительное, иначе ширина и высота в ноль,
          и сообщение создана фигура величины 0".
          Не будет ли такой подход маскировкой ошибки?
        */
    }
// на 3 параметра
    Rectangle (double x,double y,double width){
        super( x, y);
        this.width = width;
        assert width > 0;
    }

//методы

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    double getPerimetr() {
        return 2*(width+height);
    }
}
