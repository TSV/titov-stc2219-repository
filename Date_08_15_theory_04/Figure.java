package Date_08_15_theory_04;


public abstract class Figure {
//поля
    private double x; double y; //доступны только в Figure

//конструкторы
    Figure(double x, double y){  //консрруктор доступен в своём пакете
        this.x = x; this.y=y;
    }

//методы
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    abstract double getPerimetr();
}
