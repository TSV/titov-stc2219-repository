package Date_08_15_theory_04;

import java.util.Random;

public class Main {
    static Random vbr = new Random();

    public static void main(String[] args) {
        Figure[] figures = new Figure[4];
        figures[0] = new Rectangle(5,6,7,8);
        figures[1] = new Square(-4,11,45);
        figures[2] = new Ellipse(15,-62,8,7);//  ставил минусы в ширину-высоту ,assert не вызвал ошибки
        figures[3] = new Circle(-7,-26,16);
        for (int i = 0; i < figures.length; i++) {
            outputter(figures[i],i);
        }
        System.out.println();
        System.out.println("*** Demonstration of the change of coordinates ***");System.out.println();
        Movable[] bums = new Movable[2];
        bums[0] = new Square(11,22,33);
        bums[1] =new Circle(-13,17,9);
        for (int i = 0; i < bums.length; i++) {
            System.out.println("Bum " + i);
            bums[i].move(vbr.nextDouble()*vbr.nextInt(101),vbr.nextDouble()*vbr.nextInt(101));
            System.out.println();
        }
        System.out.println("Square area");
        Square squarechnic = new Square(7,8,15);
        System.out.println(squarechnic.getWidth()* squarechnic.getHeight());
    }
    private static  void outputter(Figure figure, int i){
        System.out.println("Ortocenter figure "+i+" : X= "+ figure.getX()+" Y= "+
                            figure.getY()+" perimeter = "+ figure.getPerimetr());
    }
}
