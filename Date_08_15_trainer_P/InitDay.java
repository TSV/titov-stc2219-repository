package Date_08_15_trainer_P;

public class InitDay {
    static String[] words;
    static int[] incom = {1, 1, 1};
    static String cdr;
    static ChecStartDate checStartDate = new ChecStartDate();

    public static void init() {
        System.out.println("Ожидается начальная дата события. Введите ДД/ММ/ГОД/");
        Main.userAction = Main.kb.nextLine();
        String[] tmp = splitter(Main.userAction);
        checker(words); // этот метод появился благодаря юнитам
        System.out.println("Выберите желаемый календарь. ");
        System.out.println("Григорианский - g, Хиджра - x");
        System.out.println("Другие пока неизвестны. По умолчанию будет выбран григорианский");

        cdr = Main.kb.nextLine();
        if (cdr.equals("x")) cdr = "xij";
        else cdr = "grig";
        checStartDate.checkyear(incom[0], incom[2], cdr);
        incom[1] = checStartDate.checkmonth(incom[1]);
        incom[0] = checStartDate.checkday(incom[0], incom[1]);
    }

    // этот метод появился благодаря юнитам
    //и в нём найден косяк с обработкой смеси "правильных" и "неправильных" данных
    static void checker(String[] words) {
        if (words.length == 3) {
            try {
                incom[0] = Integer.parseInt(words[0]);
            } catch (NumberFormatException e) {
                System.out.println("введена неподходящая строка. Будет использовано 1/1/1");
                incom[0] = -1;
            }
            try {
                incom[1] = Integer.parseInt(words[1]);
            } catch (NumberFormatException e) {
                System.out.println("введена неподходящая строка. Будет использовано 1/1/1");
                incom[1] = -1;
            }
            try {
                incom[2] = Integer.parseInt(words[2]);
            } catch (NumberFormatException e) {
                System.out.println("введена неподходящая строка. Будет использовано 1/1/1");
                incom[2] = -1;
            }
            //косяк лечится этой строкой
            if (incom[0]==-1 || incom[1]==-1 || incom[2]==-1){incom[0]=1;incom[1]=1;incom[2]=1;}

        } else System.out.println("введена неподходящая строка. Будет использовано 1/1/1");
    }

    public static String[] splitter(String text) {
        words = text.split("/");
        return words;
    }
}
