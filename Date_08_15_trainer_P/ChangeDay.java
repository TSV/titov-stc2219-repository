package Date_08_15_trainer_P;

import java.util.Arrays;

public class ChangeDay {

    static int[] monthlen = new int[12];

    public static void changa(DerTag day, String userAction) {
        if ((day.getCdr()).equals("xij")) {
            monthlen = Arrays.copyOf(Xijra.monthlen, 12);
            if (Xijra.checkLeap(day.getYear())) monthlen[11] += 1;
        } else {
            monthlen = Arrays.copyOf(Grigor.monthlen, 12);
            if (Grigor.checkLeap(day.getYear())) {
                monthlen[1] += 1;
            }
        }


        if (userAction.equals("+")) {
            if (day.getDay() != monthlen[day.getMonth() - 1]) day.setDay(day.getDay() + 1);
            else {
                day.setDay(1);
                if ((day.getMonth()) != 12) {
                    day.setMonth(day.getMonth() + 1);
                } else {
                    day.setMonth(1);
                    day.setYear(day.getYear() + 1);
                }
            }
        } else if (userAction.equals("-")) {
            if (day.getDay() != 1) day.setDay(day.getDay() - 1);
            else {
                if ((day.getMonth()) != 1) {
                    day.setMonth(day.getMonth() - 1);
                    day.setDay(monthlen[day.getMonth() - 1]);
                } else {
                    day.setMonth(12);
                    day.setYear(day.getYear() - 1);
                    day.setDay(monthlen[day.getMonth() - 1]);
                }
            }
        }
    }
}
