package Date_08_15_trainer_P;

public class DerTag {
    //поля
    private int day, year,month;
    private String cdr;
    //конструктор
    // благодаря юнитам отсюда часть кода ушла в другие классы
    DerTag(int day, int month,int year, String cdr){
        this.year = year;
        this.cdr = cdr;
        this.month = month;
        this.day = day;
    }
    // заготовка для 3- параметров if (cdr != "null")
    //методы

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getCdr() {
        return cdr;
    }

    public void setCdr(String cdr) {
        this.cdr = cdr;
    }


}
