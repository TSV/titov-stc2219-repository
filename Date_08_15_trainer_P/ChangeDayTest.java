package Date_08_15_trainer_P;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class ChangeDayTest {
    ChangeDay chD1 = new ChangeDay();
    //Тест проверяет сумму день + месяц =год
    //на вход тесту подаётся адекватная дата, т.к. в основной
    //программе адекватность полей дня проверяется другими средствами

    @Test
    void changa1(){
        DerTag testday = new DerTag(31,12,2022,"grig");
        chD1.changa(testday,"+");
        int result =  testday.getDay()+testday.getMonth()+testday.getYear();
        assert result == 2025;
    }
    @Test
    void changa2(){
        DerTag testday = new DerTag(31,12,2022,"grig");
        chD1.changa(testday,"-");
        int result =  testday.getDay()+testday.getMonth()+testday.getYear();
        assert result == 2064;
    }
    @Test
    void changa3(){
        DerTag testday = new DerTag(29,12,1477,"xij");
        chD1.changa(testday,"-");
        int result =  testday.getDay()+testday.getMonth()+testday.getYear();
        assert result == 1517;
    }

    @Test
    void changa4(){
        DerTag testday = new DerTag(29,12,1477,"xij");
        chD1.changa(testday,"+");
        int result =  testday.getDay()+testday.getMonth()+testday.getYear();
        assert result == 1480;
    }

}