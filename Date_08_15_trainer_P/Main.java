package Date_08_15_trainer_P;

import java.util.Arrays;
import java.util.Scanner;
// модель установщика даты на устройстве (или где-то ещё)
// от пользователя требуется любая текстовая строка
// строка проверяется на предмет: можно ли из неё сделать дату
// на основании анализа строки выставляется начальная дата
// есть инструмент добавить/уменьшиить день на 1 шаг
// обработка перехода в "до нашей эры" не предусмотрена
// т.к. должна включать в себя дугой алгоритмм проверки года на високосность
// а следовательно, учёт ситуации 1-й год -1 = -1 год (1-й год до гашей эры) не актуален
// алгоритм вернёт 0-й год


public class Main {
    static String userAction;
    static Scanner kb = new Scanner(System.in);
    public static void main(String[] args) {

        String[] monthName = new String[12];
        InitDay initDay = new InitDay();
        initDay.init();
        DerTag dayone = new DerTag(initDay.incom[0], initDay.incom[1], initDay.incom[2], initDay.cdr);
        if ((dayone.getCdr()).equals("grig")) {
            monthName = Arrays.copyOf(Grigor.month, 12);
        } else if ((dayone.getCdr()).equals("xij")) {
            monthName = Arrays.copyOf(Xijra.month, 12);
        }

        System.out.println("Установлена дата  " + dayone.getDay() + "  " +
                monthName[dayone.getMonth() - 1] + " " + dayone.getYear() + "  года.");

        System.out.println("Что мы сделаем с этим? Варианты:");
        System.out.println("день вперёд  -  нажмите +");
        System.out.println("день назад -  нажмите -");
        System.out.println("ничего не делать -  нажмите что-нибудь другое");
        userAction = kb.nextLine();
        ChangeDay.changa(dayone,userAction);
        System.out.println("Новая дата:  " + dayone.getDay() + "  " +
                monthName[dayone.getMonth() - 1] + " " + dayone.getYear() + "  года.");



    }
}
