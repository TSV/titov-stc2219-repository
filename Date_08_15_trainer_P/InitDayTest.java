package Date_08_15_trainer_P;

import org.junit.jupiter.api.Test;

class InitDayTest {
    InitDay t1 = new InitDay();

    @Test
    void splitter() {

        String[] testword = t1.splitter("11/12/13");
        String restest = "";
        for (int i = 0; i < testword.length; i++) {
            restest += testword[testword.length-i-1]+" ";
        }
        assert restest.equals("13 12 11 ");
    }

    @Test
    void splitter1() {

        String[] testword = t1.splitter("kryakryakrya");
        String restest = "";
        for (int i = 0; i < testword.length; i++) {
            restest += testword[testword.length-i-1]+" ";
        }
        assert restest.equals("kryakryakrya ");
    }
    @Test
    void splitter2() {

        String[] testword = t1.splitter("myau/gav/be");
        String restest = "";
        for (int i = 0; i < testword.length; i++) {
            restest += testword[testword.length-i-1]+" ";
        }
        assert restest.equals("be gav myau ");
    }
    @Test
    void checker(){
        String[] words ={"4","5","6"};
        int rez = 0;
        t1.checker(words);
        for (int i = 0; i < 3; i++) {
            rez += t1.incom[i];
        }
        assert rez == 15;
    }
    @Test
    void checker1(){
        String[] words ={"xru","krya","21"};
        int rez = 0;
        t1.checker(words);
        for (int i = 0; i < 3; i++) {
            rez += t1.incom[i];
        }
        assert rez == 3;
    }
}