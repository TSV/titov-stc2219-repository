package Date_09_16_theory_21;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        UserRepositoryFileImpl users = new UserRepositoryFileImpl();
        System.out.println("Начнём с номера "+users.counter);
        users.addUser("Krorodile","Gena",100,false);
        users.addUser("Uylabdy","Usem",77,false);
        users.addUser("Мари Жанна","Бекю",77,false);
        users.addUser("Uychul","Usem",33,true);
        users.addUser("Конрад","Михельсон",48,true);
        users.addUser("Израэль","Хенс",26,true);
        users.addUser("Мари Хуанна","Младшая",26,true);
        System.out.println("Завершили номером "+users.counter);

        try {users.findById(14).printUsverInfo();
        } catch (NullPointerException e){
            System.out.println("Такой id не найден");
        }

        users.delete(30);
        users.update(users.findById(15));
        //System.out.println(users.randomName());

    }
}
