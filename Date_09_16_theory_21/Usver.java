package Date_09_16_theory_21;

public class Usver {

    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private boolean employed;
    private static final String rgx = "|";

    public Usver(int id, String firstName, String lastName, int age, boolean employed) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.employed = employed;
    }
    public Usver(int id){
        this.id = id;
        this.firstName = "Неизвестный";
        this.lastName = "Неизвестный";
        this.age = 0;
        this.employed = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isEmployed() {
        return employed;
    }

    public void setEmployed(boolean employed) {
        this.employed = employed;
    }
    public void printUsverInfo(){
        System.out.println("id = "+id +" "+firstName+" "+lastName+" "+age+" "+employed);
    }
    public String toBase(){
        return id + rgx +firstName+ rgx +lastName+ rgx +age+ rgx +employed;
    }

}
