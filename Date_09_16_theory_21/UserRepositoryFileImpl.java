package Date_09_16_theory_21;

import java.io.*;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;

public class UserRepositoryFileImpl {
    public static int counter = 0;
    private final String dirPath = "Date_09_16_theory_21\\data";
    private final String fileName = "data.txt";
    private final File dir = new File(dirPath);
    private final File data = new File(dirPath + "\\" + fileName);

    private final String rgx = "\\|";

    public UserRepositoryFileImpl() {
        initData();
        checkData();
    }

    private boolean isDirExist(File dir) {
        return dir.exists() && dir.isDirectory();
    }

    private boolean isFileExist(File data) {
        return data.exists() && data.isFile();
    }

    private void initData() {
        if (!isDirExist(dir)) {
            new File(dirPath).mkdir();
            try {
                if (data.createNewFile()) {
                    System.out.println("Создан новый файл данных");
                }
            } catch (IOException e) {
                throw new RuntimeException();
            }
        } else if (!isFileExist(data)) {
            try {
                if (data.createNewFile()) {
                    System.out.println("Создан новый файл данных");
                }
            } catch (IOException e) {
                throw new RuntimeException();
            }
        }
    }

    private void checkData() {
        int cnt = 0;
        try (Reader reader = new FileReader(dirPath + "\\" + fileName)) {
            BufferedReader bufferedReader = new BufferedReader(reader);
            String currentStr = bufferedReader.readLine();
            String outStr;
            while (currentStr != null) {
                int currentId = 0;
                outStr = currentStr;
                String[] record = outStr.split(rgx);
                try {
                    currentId = Integer.parseInt(record[0]);
                } catch (NumberFormatException e) {
                    System.out.println("Data file Error");
                    throw new RuntimeException();
                }
                if (currentId > cnt) {
                    cnt = currentId;
                }
                currentStr = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new RuntimeException();
        }
        counter = cnt;
    }

    public void addUser(String fName, String lName, int age, boolean empl) {
        Usver newcomer = new Usver(counter + 1, fName, lName, age, empl);
        counter += 1;
        create(newcomer);
    }

    private void create(Usver usver) {
        String userToString = usver.toBase() + "\n";
        try (Writer writer = new FileWriter(data, true)) {
            writer.write(userToString);
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    void update(Usver usver) {
        try{int consumable = usver.getId();
        delete(consumable);
        usver.setFirstName(randomName());
        usver.setLastName(randomName());
        System.out.printf("для id = %d внесены изменения: %s  ",consumable,usver.toBase());
        create(usver);
        } catch (NullPointerException e){
            System.out.println("Персонаж не найден, изменения не внесены");
        }
        }

    void delete(int id) {
        HashMap<Integer, Usver> temp = getUvsers();
        if (temp.containsKey(id)) {
            temp.remove(id);
            data.delete();
            initData();
            Set<Integer> keys = temp.keySet();
            for (int key : keys) {
                create(temp.get(key));
            }
            System.out.printf("id = %d успешно удалён \n",id);
        } else {
            System.out.printf("id = %d не обнаружен. Измения не внесены.",id);
        }
    }

    public Usver findByIdAlt(int id) {
        Usver unlost = null;
        try (Reader reader = new FileReader(data)) {
            BufferedReader bufferedReader = new BufferedReader(reader);
            String record = bufferedReader.readLine();
            while (record != null) {
                String[] fields = record.split(rgx);
                if (Integer.parseInt(fields[0]) == id) {
                    unlost = new Usver(Integer.parseInt(fields[0]), fields[2], fields[4],
                            Integer.parseInt(fields[6]), Boolean.parseBoolean(fields[8]));
                    break;
                }
                record = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new RuntimeException();
        }
        return unlost;
    }

    public Usver findById(int id) {
        Usver unlost = null;
        HashMap<Integer, Usver> temp = getUvsers();
        unlost = temp.containsKey(id) ? temp.get(id) : null;
        return unlost;
    }

    private HashMap<Integer, Usver> getUvsers() {
        HashMap<Integer, Usver> temp = new HashMap<>();
        Usver current = null;
        int currentId;
        try (Reader reader = new FileReader(data)) {
            BufferedReader bufferedReader = new BufferedReader(reader);
            String record = bufferedReader.readLine();
            while (record != null) {
                String[] fields = record.split(rgx);

                current = new Usver(Integer.parseInt(fields[0]), fields[1], fields[2],
                        Integer.parseInt(fields[3]), Boolean.parseBoolean(fields[4]));
                currentId = current.getId();
                temp.put(currentId, current);
                record = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new RuntimeException();
        }
        return temp;
    }
    public String randomName(){
        Random rnd = new Random();
        Random vbr = new Random();
        String word = (Character.toString((char) (rnd.nextInt(32)+(int) 'А')));
        for (int i = 0; i < (vbr.nextInt(12)+1); i++) {
            word+=Character.toString((char) (rnd.nextInt(32)+(int) 'а'));
        }
        return word;
    }
}
