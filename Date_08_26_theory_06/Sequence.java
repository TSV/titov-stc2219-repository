package Date_08_26_theory_06;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Sequence {
    public static void main(String[] args) {
//        int[] originalArray = {2468642,10200201,987656789,132};
//
        //создание массива тожне залямбдим
        int[] originalArray = getArray(() -> {
            Random rnd = new Random();
            int[] argo = new int[rnd.nextInt(10000)];
            for (int i = 0; i < argo.length; i++) {
                argo[i] = rnd.nextInt(rnd.nextInt(Integer.MAX_VALUE));
            }
            return argo;
        });
        System.out.println("Исходный массив");
        System.out.println(Arrays.toString(originalArray));

        ByCondition condeven = number -> number % 2 == 0;

        ByCondition condsumello = number -> cifiro(number)%2 == 0;// интересно, как в лямбде органимзовать рекурсию?

        ByCondition allCifiroEven = number -> {
            boolean result = true;
            while (number>0){if ((number%10)%2 !=0) {result = false;break;} number/=10;}
            return result;
        };

        ByCondition palindromo = number -> {
            int result = 0; int tmp = number;
            while (tmp>0){result = result*10+tmp%10;tmp/=10;}
            return result == number;
        };

        System.out.println("Чётные из исходного");
        System.out.println(Arrays.toString(filter(originalArray, condeven)));
        System.out.println("Сумма цифр чётная");
        System.out.println(Arrays.toString(filter(originalArray, condsumello)));
        System.out.println("Все цифры чётные");
        System.out.println(Arrays.toString(filter(originalArray, allCifiroEven)));
        System.out.println("Палиндромы");
        System.out.println(Arrays.toString(filter(originalArray, palindromo)));

    }

    // конец main
    public static int[] filter(int[] array, ByCondition condition) {
        ArrayList<Integer> temp = new ArrayList<>();
        int count = 0;
        for (int j : array) {
            if (condition.isOk(j)) {
                temp.add(j);
                count += 1;
            }
        }
        int [] result = new int[count];
        for (int i = 0; i < count; i++) {
            result[i] = temp.get(i);
        }
        return result;
    }

    public static int[] getArray(Arraymizer arraymizer) {
        return arraymizer.getSomeArray();
    }
    public static int cifiro(int num){ //это для опыта в рекурсиях
        int result = num%10;
        result += (num > 0 ? cifiro(num/10):0);
        //if (num > 0) result += cifiro(num/10);
        return result;
    }
}

