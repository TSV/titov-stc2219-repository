package Date_08_26_theory_06;

import org.junit.jupiter.api.Test;

import java.util.Arrays;


class SequenceTest {

    private static final int[] originalArray = {2468642,10200201,987656789,132};
    @Test
    void filterTest1() {
        //int[] originalArray = {2468642,10200201,987656789,132};
        ByCondition condeven = number -> number % 2 == 0;
        int[] result = Sequence.filter(originalArray, condeven);
        int[] expected = {2468642,132};
        System.out.println(Arrays.toString(result)+" "+Arrays.toString(expected));
        assert Arrays.equals(result, expected);

    }
    @Test
    void filterTest2() {
        //int[] originalArray = {2468642,10200201,987656789,132};
            ByCondition condsumello = number -> Sequence.cifiro(number)%2 == 0; // интересно, как в лямбде органимзовать рекурсию?

        int[] result = Sequence.filter(originalArray, condsumello);
        int[] expected = {2468642,10200201,132};
        System.out.println(Arrays.toString(result)+" "+Arrays.toString(expected));
        assert Arrays.equals(result, expected);

    }
    @Test
    void filterTest3() {
        //int[] originalArray = {2468642,10200201,987656789,132};
        ByCondition allCifiroEven = number -> {
            boolean result = true;
            while (number>0){if ((number%10)%2 !=0) {result = false;break;} number/=10;}
            return result;
        };
        int[] result = Sequence.filter(originalArray, allCifiroEven);
        int[] expected = {2468642};
        System.out.println(Arrays.toString(result)+" "+Arrays.toString(expected));
        assert Arrays.equals(result, expected);

    }

    @Test
    void filterTest4() {
        //int[] originalArray = {2468642,10200201,987656789,132};
        ByCondition palindromo = number -> {
            int result = 0; int tmp = number;
            while (tmp>0){result = result*10+tmp%10;tmp/=10;}
            return result == number;
        };

        int[] result = Sequence.filter(originalArray, palindromo);
        int[] expected = {2468642,10200201,987656789};
        System.out.println(Arrays.toString(result)+" "+Arrays.toString(expected));
        assert Arrays.equals(result,expected);

    }
    @Test
     void cifiroTest1(){
        int num = 12345;
        assert Sequence.cifiro(num) == 15;
    }
    @Test
     void cifiroTest2(){
        int num = 1020030045;
        assert Sequence.cifiro(num) == 15;
    }
}